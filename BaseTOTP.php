<?php

/**
 *  _   __ __ _____ _____ ___  ____  _____
 * | | / // // ___//_  _//   ||  __||_   _|
 * | |/ // /(__  )  / / / /| || |     | |
 * |___//_//____/  /_/ /_/ |_||_|     |_|
 * @link https://vistart.name/
 * @copyright Copyright (c) 2016 vistart
 * @license https://vistart.name/license/
 */

namespace rhosocial\helpers\totp;

/**
 * Description of TOTP
 *
 * @author vistart <i@vistart.name>
 */
class BaseTOTP
{

    /**
     * Returns the current Unix Timestamp devided by the timeStep
     * period.
     * @return integer
     * */
    public function getTimestamp()
    {
        return floor(microtime(true) / $this->timeStep);
    }

    /**
     * @var int time step in seconds.
     */
    public $timeStep = 30;

    /**
     * @var int the length of the hashed key. the minimum length is 6.
     */
    public $digits = 6;

    public function Google2FAGenerate(string $key): string
    {
        return $this->generate(HOTP::base32_decode($key));
    }

    public function generate(string $key, $useTimeStamp = true, string $crypto = 'sha1'): string
    {
        $timeStamp = $this->getTimestamp();

        if ($useTimeStamp !== true)
            $timeStamp = (int) $useTimeStamp;

        return HOTP::generate($key, $timeStamp, $this->digits, $crypto);
    }

    /**
     * Verifys a user inputted key against the current timestamp. Checks $window
     * keys either side of the timestamp.
     *
     * @param string $b32seed
     * @param string $key - User specified key
     * @param integer $window
     * @param boolean $useTimeStamp
     * @return boolean
     * */
    public function Google2FAVerify($b32seed, $key, $window = 5, $useTimeStamp = true)
    {
        return $this->verify(HOTP::base32_decode($b32seed), $key, $window, $useTimeStamp);
    }

    /**
     * Verifys a user inputted key against the current timestamp. Checks $window
     * keys either side of the timestamp.
     *
     * @param string $seed
     * @param string $key - User specified key
     * @param integer $window
     * @param boolean $useTimeStamp
     * @return boolean
     * */
    public function verify($seed, $key, $window = 29, $useTimeStamp = true, $crypto = 'sha1')
    {

        $timeStamp = $this->getTimestamp();

        if ($useTimeStamp !== true)
            $timeStamp = (int) $useTimeStamp;

        for ($ts = $timeStamp - $window; $ts <= $timeStamp + $window; $ts += 5) {
            if ($this->generate($seed, $timeStamp, $crypto) == $key)
                return true;
        }
        return false;
    }
}
