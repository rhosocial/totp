<?php

/**
 *  _   __ __ _____ _____ ___  ____  _____
 * | | / // // ___//_  _//   ||  __||_   _|
 * | |/ // /(__  )  / / / /| || |     | |
 * |___//_//____/  /_/ /_/ |_||_|     |_|
 * @link https://vistart.name/
 * @copyright Copyright (c) 2016 vistart
 * @license https://vistart.name/license/
 */

require_once('bootstrap.php');

$totp = new rhosocial\helpers\totp\TOTP();
echo ($key = $totp->Google2FAGenerate("LFLFMU2SGVCUIUCZKBMEKRKLIQ")) . "\n";
echo ($totp->Google2FAVerify("LFLFMU2SGVCUIUCZKBMEKRKLIQ", $key));
