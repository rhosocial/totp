<?php

/**
 *  _   __ __ _____ _____ ___  ____  _____
 * | | / // // ___//_  _//   ||  __||_   _|
 * | |/ // /(__  )  / / / /| || |     | |
 * |___//_//____/  /_/ /_/ |_||_|     |_|
 * @link https://vistart.name/
 * @copyright Copyright (c) 2016 vistart
 * @license https://vistart.name/license/
 */

namespace rhosocial\helpers\totp;

/**
 * Description of HOTP
 *
 * @author vistart <i@vistart.name>
 */
class BaseHOTP
{

    private static $lut = array(// Lookup needed for Base32 encoding
        "A" => 0, "B" => 1,
        "C" => 2, "D" => 3,
        "E" => 4, "F" => 5,
        "G" => 6, "H" => 7,
        "I" => 8, "J" => 9,
        "K" => 10, "L" => 11,
        "M" => 12, "N" => 13,
        "O" => 14, "P" => 15,
        "Q" => 16, "R" => 17,
        "S" => 18, "T" => 19,
        "U" => 20, "V" => 21,
        "W" => 22, "X" => 23,
        "Y" => 24, "Z" => 25,
        "2" => 26, "3" => 27,
        "4" => 28, "5" => 29,
        "6" => 30, "7" => 31
    );

    /**
     * Generates a 16 digit secret key in base32 format
     * @return string
     * */
    public static function generate_secret_key($length = 16)
    {
        $b32 = "234567QWERTYUIOPASDFGHJKLZXCVBNM";
        $s = "";

        for ($i = 0; $i < $length; $i++)
            $s .= $b32[rand(0, 31)];

        return $s;
    }

    /**
     * Decodes a base32 string into a binary string.
     * */
    public static function base32_decode($b32)
    {

        $b32 = strtoupper($b32);

        if (!preg_match('/^[ABCDEFGHIJKLMNOPQRSTUVWXYZ234567]+$/', $b32, $match))
            throw new \Exception('Invalid characters in the base32 string.');

        $l = strlen($b32);
        $n = 0;
        $j = 0;
        $binary = "";

        for ($i = 0; $i < $l; $i++) {

            $n = $n << 5;     // Move buffer left by 5 to make room
            $n = $n + self::$lut[$b32[$i]];  // Add value into buffer
            $j = $j + 5;    // Keep track of number of bits in buffer

            if ($j >= 8) {
                $j = $j - 8;
                $binary .= chr(($n & (0xFF << $j)) >> $j);
            }
        }

        return $binary;
    }

    public static function base32_encode($data, $length)
    {
        $basestr = "ABCDEFGHIJKLMNOPQRSTUVWXYZ234567";
        $count = 0;
        if ($length > 0) {
            $buffer = $data[0];
            $next = 1;
            $bitsLeft = 8;

            while (($bitsLeft > 0 || $next < $length)) {
                if ($bitsLeft < 5) {
                    if ($next < $length) {
                        $buffer <<= 8;
                        $buffer |= $data[$next++] & 0xFF;
                        $bitsLeft += 8;
                    } else {
                        $pad = 5 - $bitsLeft;
                        $buffer <<= $pad;
                        $bitsLeft += $pad;
                    }
                }
                $index = 0x1F & ($buffer >> ($bitsLeft - 5));
                $bitsLeft -= 5;
                $result .= $basestr[$index];
                $count++;
            }
        }
        return $result;
    }

    /**
     * Takes the secret key and the timestamp and returns the one time
     * password.
     *
     * @param binary $key - Secret key in binary form.
     * @param integer $counter - Timestamp as returned by get_timestamp.
     * @return string
     * */
    public static function generate($key, $counter, $digits = 6, $crypto = 'sha1')
    {
        if (strlen($key) < 8)
            throw new \Exception('Secret key is too short. Must be at least 16 base 32 characters');

        $bin_counter = pack('N*', 0) . pack('N*', $counter);  // Counter must be 64-bit int
        $hash = hash_hmac($crypto, $bin_counter, $key, true);
        return str_pad(self::truncate($hash, $digits), $digits, '0', STR_PAD_LEFT);
    }

    /**
     * Extracts the OTP from the SHA1 hash.
     * @param binary $hash
     * @return integer
     * */
    public static function truncate($hash, $digits)
    {
        $offset = ord($hash[19]) & 0xf;

        return (
            ((ord($hash[$offset + 0]) & 0x7f) << 24 ) |
            ((ord($hash[$offset + 1]) & 0xff) << 16 ) |
            ((ord($hash[$offset + 2]) & 0xff) << 8 ) |
            (ord($hash[$offset + 3]) & 0xff)
            ) % pow(10, $digits);
    }
}
